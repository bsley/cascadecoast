// $(document).ready(function() {

// // BACK TO TOP BUTTON

// $('.BackToTop').click(function(){
//     $('html, body').animate({scrollTop : 0},200);
//     return false;
//   });

// // FORCE LINKS IN BLOG POST TO OPEN IN BLANK window

// $('.BlogContainer p a').attr('target', '_blank');

// // STAGGER IN PHOTOS

// $('.featphoto, .EventsThumbnail').velocity("transition.slideDownIn", {
//         delay: 200,
//         duration: 500,
//         stagger: 500,
// });

// // AUTO TAB FUNCTIONALITY IN DONATE

// $('.autotab').keypress(function (event) {

//     var maxLen = this.maxLength;
//     var currentLen = this.value.length;
//     var numberKeys = "1,2,3,4,5,6,7,8,9,0";
//     var key = String.fromCharCode(event.which);
//     console.log("key pressed");

//     if (numberKeys.indexOf(key) != -1) {
//       if (maxLen === currentLen) {
//         //$(this).next().focus();
//         $(this).parent().next().find('input').focus();
//         console.log("next!")
//       }
//     }
// });

// // $('.DonateCheckbox').change(function() {
// //     $('.DonatePerson').toggleClass("open");
// // });

//   /* LINKING ENTIRE DIVS */

//   $(".linked").click(function(){
//     window.location=$(this).find("a").attr("href");
//     console.log('clicked');
//     return false;
//   });


//   $(".CardClose").click(function(){
//     $(this).parent().addClass("off");
//   });

//   // MENU FUNCTIONALITY

//   var menuHit = $(".menuHit");
//   var nav = $("nav.miniMenu");
//   var navItems = $("nav li");
//   var body = $("body");
//   var logo = $(".logo");
//   var menuStatus = false;

//   menuHit.hammer().bind("tap", function() {

//     if (!menuStatus) {
//       menuStatus = true;
//       console.log("tapped and menu is now on");
//       $(this).find(".menuBtn div").addClass('open');
//       nav.removeClass('hidden visuallyhidden');
//       body.addClass('locked');
//       $('.DonateButtonBigParent').addClass('open');
//       // logo.addClass('hidden');
//       navItems.velocity("transition.fadeIn", {
// 		  	duration: 200,
// 		  	stagger: 20,
// 		});
//      event.preventDefault();

//     } else {
//       menuStatus = false;
//       console.log("tapped and menu is now off")
//       $(this).find(".menuBtn div").removeClass('open');
//       nav.addClass('hidden visuallyhidden');
//       body.removeClass('locked');
//       $('.DonateButtonBigParent').removeClass('open');
//       // logo.removeClass('hidden');
//       navItems.hide();
//     }

//   });

// // DONATE - POPULATES SUBMIT BUTTON WITH AMOUNT FROM FIELD

// $(".DonateAmount").keyup(function () {
//    var value = $(this).val();
//    if (value) {
//      $(".DonateSubmit").text("Give $" + value);
//    }
//    else {
//      $(".DonateSubmit").text("Give");
//    };
//    //console.log(value);
//  }).keyup();

// // DONATE - POPULATES CONFIRMATION WITH AMOUNT AND NAME

// $(".DonateAmount").keyup(function () {
//    var value = $(this).val();
//    if (value) {
//      $(".DonateConfirmationAmount").text(value);
//    }
//  }).keyup();

// $(".DonateName").keyup(function () {
//    var value = $(this).val();
//    var FirstName = value.split(" ", 1);
//    if (value) {
//      $(".DonateConfirmationName").text(FirstName);
//    }
//  }).keyup();

// $(".DonateEmail").keyup(function () {
//    var value = $(this).val();
//    if (value) {
//      $(".DonateConfirmationEmail").text(value);
//    }
//  }).keyup();

// // TEMP TOOL TO SHOW CONFIRMATION WITHOUT FORM SUBMISSION

// // $(".DonateSubmit").click(function(){
// //   $('.DonateConfirmation').css("display", "block");
// //   $('.DonateConfirmation').css("opacity", "1");
// // });



// // This identifies your website in the createToken call below
//   Stripe.setPublishableKey('pk_test_AyC9dEKTqaQgwyYcPduchVLz');

//   var stripeResponseHandler = function(status, response) {
//     var $form = $('#payment-form');

//     if (response.error) {
//       // Show the errors on the form
//       $form.find('.payment-errors').html("<p class='DonateError'>" + response.error.message + "</p>");
//       $form.find('button').prop('disabled', false);
//     } else {
//       // token contains id, last4, and card type
//       var token = response.id;
//       // Insert the token into the form so it gets submitted to the server
//       $form.append($('<input type="hidden" name="stripeToken" />').val(token));
//       // and re-submit
//       $form.get(0).submit();
//     }
//   };

//   jQuery(function($) {
//     $('#payment-form').submit(function(e) {

//       var $form = $(this);
//       var $rqrd = $(this).find('.rqrd');

//       $rqrd.each(function(index, el) {

//           var $t = $(this);

//           if ($t.val() === '') {
//               $t.addClass('error');
//           }else{
//               $t.removeClass('error');
//           }

//       });

//       if ($('.rqrd.error').length === 0) {
//           // Disable the submit button to prevent repeated clicks
//           $form.find('button').prop('disabled', true);
//           Stripe.card.createToken($form, stripeResponseHandler);
//       }else{
//           $('.payment-errors').html('<p class="DonateError">Please input required fields (marked red)</p>');
//       }

//       // Prevent the form from submitting with the default action
//       return false;
//     });
//   });


// });
